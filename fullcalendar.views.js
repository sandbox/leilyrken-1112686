(function ($) {

Drupal.behaviors.fullCalendar = function(context) {
  $('#fullcalendar-content').hide(); //hide the failover display
  var fullcalendar = $('#fullcalendar:not(.fc-processed)').addClass('fc-processed').fullCalendar({
    defaultView: Drupal.settings.fullcalendar.defaultView,
    theme: Drupal.settings.fullcalendar.theme,
    header: {
      left: Drupal.settings.fullcalendar.left,
      center: Drupal.settings.fullcalendar.center,
      right: Drupal.settings.fullcalendar.right
    },
    eventClick: function(calEvent, jsEvent, view) {
      if (Drupal.settings.fullcalendar.colorbox) {
      // Open in colorbox if exists, else open in new window.
        if ($.colorbox) {
          $.colorbox({href:calEvent.url, iframe:true, width:'80%', height:'80%'});
        } else {
          window.open(calEvent.url);
        }
      } else {
        window.open(calEvent.url);
      }
      return false;
    },
    year: (Drupal.settings.fullcalendar.year) ? Drupal.settings.fullcalendar.year : undefined,
    month: (Drupal.settings.fullcalendar.month) ? Drupal.settings.fullcalendar.month : undefined,
    day: (Drupal.settings.fullcalendar.day) ? Drupal.settings.fullcalendar.day : undefined,
    timeFormat: {
      agenda: (Drupal.settings.fullcalendar.clock) ? 'HH:mm{ - HH:mm}' : Drupal.settings.fullcalendar.agenda,
      '': (Drupal.settings.fullcalendar.clock) ? 'HH:mm' : 'h(:mm)t'
    },
    axisFormat: (Drupal.settings.fullcalendar.clock) ? 'HH:mm' : 'h(:mm)tt',
    weekMode: Drupal.settings.fullcalendar.weekMode,
    firstDay: Drupal.settings.fullcalendar.firstDay,
    monthNames: Drupal.settings.fullcalendar.monthNames,
    monthNamesShort: Drupal.settings.fullcalendar.monthNamesShort,
    dayNames: Drupal.settings.fullcalendar.dayNames,
    dayNamesShort: Drupal.settings.fullcalendar.dayNamesShort,
    allDayText: Drupal.settings.fullcalendar.allDayText,
    buttonText: {
      today:  Drupal.settings.fullcalendar.todayString,
      day: Drupal.settings.fullcalendar.dayString,
      week: Drupal.settings.fullcalendar.weekString,
      month: Drupal.settings.fullcalendar.monthString
    },
    events: function(start, end, callback) {
      var events = [];

      $('.fullcalendar_event').each(function() {
        $(this).find('.fullcalendar_event_details').each(function() {
          events.push({
            field: $(this).attr('field'),
            index: $(this).attr('index'),
            nid: $(this).attr('nid'),
            title: $(this).attr('title'),
            start: $(this).attr('start'),
            end: $(this).attr('end'),
            url: $(this).attr('href'),
            allDay: ($(this).attr('allDay') === '1'),
            className: $(this).attr('cn'),
            editable: $(this).attr('editable'),
      deleteable: $(this).attr('deleteable'),
      type: $(this).attr('type'),
          });
        });
      });

      callback(events);
    },
    eventDrop: function(event, dayDelta, minuteDelta, allDay, revertFunc) {
      $.post(Drupal.settings.basePath + 'fullcalendar/ajax/update/drop/'+ event.nid,
        'field=' + event.field + '&index=' + event.index + '&day_delta=' + dayDelta + '&minute_delta=' + minuteDelta + '&all_day=' + allDay,
        fullcalendarUpdate);
      return false;
    },
    eventResize: function(event, dayDelta, minuteDelta, revertFunc) {
      $.post(Drupal.settings.basePath + 'fullcalendar/ajax/update/resize/'+ event.nid,
        'field=' + event.field + '&index=' + event.index + '&day_delta=' + dayDelta + '&minute_delta=' + minuteDelta,
        fullcalendarUpdate);
      return false;
    },
  /* Add create on select */  
  selectable: Drupal.settings.fullcalendar.createPermission,
  selectHelper: Drupal.settings.fullcalendar.createPermission,
  select: function(start, end, allDay) {
    if(Drupal.settings.fullcalendar.createPermission) {
      if (Drupal.settings.fullcalendar.colorbox) {
        if ($.colorbox) {        
        
          var begin = Math.round(start.getTime()/1000) ;
          var finish = Math.round(end.getTime()/1000) ;
          
          $.colorbox({href:Drupal.settings.basePath + 'fullcalendar/form/'+Drupal.settings.fullcalendar.createNodeType+'/create/'+begin+'/'+finish, 
              iframe:false, 
              width:'80%', 
              height:'80%',
              onComplete:function(){
                $('#node-form').submit(function() {
                  $('#edit-submit').attr('disabled', 'disabled');
                  this.value = Drupal.t("Loading..."); 
                  $.post(
                      Drupal.settings.basePath + "fullcalendar/ajax/create/" + Drupal.settings.fullcalendar.createNodeType,  
                      { 
                        // TODO: send all the field from the form not only date and title
                        title: $("#edit-title").val(),
                        start: $("#fc-start-timestamp").val(), 
                        end: $("#fc-end-timestamp").val(), 
                        date_field: Drupal.settings.fullcalendar.nodeDateField,
                        title_field: Drupal.settings.fullcalendar.nodeTitleField,
                        url_field: Drupal.settings.fullcalendar.nodeURLField,
                      },
                      function(response) {
                        var result = Drupal.parseJson(response);
                        if(!result.status) {
                          $('#edit-submit').attr('disabled', 'enabled');
                          $('#edit-submit').attr('value', Drupal.t('Save')); // TODO: put back previous submit value
                          alert('Please select a title.'); // TODO: apply correct validation  
                        } 
                        else {
                          $('#edit-submit').attr('value',Drupal.t('Saved!')); 
                          $.fn.colorbox.close();
                          
                          var date = new Date();
                          var startDate = new Date().setTime(result.start);
                          var endDate = new Date().setTime(result.end);

                          fullcalendar.fullCalendar('renderEvent',
                            {    
                              start: result.start, 
                              end: result.end, 
                              allDay: result.allDay,
                              title: result.title,
                              nid: result.nid, 
                              url: Drupal.settings.basePath + result.url, 
                              className: result.className, 
                              editable: result.editable, 
                              deleteable: result.deleteable,
                              type: result.type, 
                            },
                            true
                          );
                          if ($('#fullcalendar-status').text() === '') {
                            $('#fullcalendar-status').html(result.msg).slideDown();
                          } else {
                            $('#fullcalendar-status').html(result.msg).effect('highlight', {}, 5000);
                          }
                        }
                        return false;          
                      }
                    );
                  return false;
                });
              }
            });
        } else {
          window.open(Drupal.settings.basePath + 'node/add/' + Drupal.settings.fullcalendar.createNodeType);
        }
      }
    }
    $('#fullcalendar.fc-processed').fullCalendar('unselect');
  }
  /* End create on select */
  /* Start right click menu to edit and delete */
  ,  
  eventRender: function(calEvent,element){
        /**/
    if(calEvent.editable || calEvent.deleteable) {
      element.find('.fc-event-title').append(
        '<img class="fc-event-action" '+
        'src="'+Drupal.settings.fullcalendar.modulePath+'/contextMenu/images/mouse-select-right.png" '+
        'alt="'+ Drupal.t('right click for special action') +'" '+ 
        'style="float: right;align:top;" />'
      );
    }
    if(calEvent.editable) {
      var editMenu = "#edit";
    } else {
      var editMenu = "";
    }
    if(calEvent.deleteable) {
      var deleteMenu = "#delete";
    } else {
      var deleteMenu = "";
    }
    element.find('.fc-event-action').contextMenu({menu: 'fullcalendar-context-menu-node'},      
      function(action, el, pos) {
        if(action =='quit') {}
        else if(action =='edit') {
          if(!calEvent.editable) {
            return;
          }
          var editEventUrl = Drupal.settings.basePath + 'fullcalendar/form/'+ calEvent.type +'/edit/' + calEvent.nid;
          if (Drupal.settings.fullcalendar.colorbox) {
            if ($.colorbox) {
              $.colorbox({
                href:editEventUrl, iframe:false, 
                width:'80%', 
                height:'80%',
                onComplete:function(){
                  $('#node-form').submit(function() {
                    $('#edit-submit').attr('disabled', 'disabled');
                    this.value = Drupal.t("Loading..."); 
                    $.post(                      
                      Drupal.settings.basePath + "fullcalendar/ajax/edit/" + calEvent.type,
                      { 
                        // TODO: send all the field from the form not only date and title
                        nid: calEvent.nid,
                        title: $("#edit-title").val(),
                        date_field: Drupal.settings.fullcalendar.nodeDateField,
                        title_field: Drupal.settings.fullcalendar.nodeTitleField,
                        // TODO: correct send title field value
                        // title_field_value: $(Drupal.settings.fullcalendar.nodeTitleField + '[0][value]').val(),
                        url_field: Drupal.settings.fullcalendar.nodeURLField,
                        // TODO: correct send url field value
                        // url_field_value: $(Drupal.settings.fullcalendar.nodeURLField + '[0][value]').val(),
                      },
                      function(response) {
                        var result = Drupal.parseJson(response);
                        if(!result.status) {
                          $('#edit-submit').attr('disabled', 'enabled');
                          $('#edit-submit').attr('value', Drupal.t('Save')); // TODO: put back previous submit value
                          alert(Drupal.t('Please select a title.'));   // TODO: remove and apply correct validation
                        } 
                        else {
                          $('#edit-submit').attr('value',Drupal.t('Saved!')); 
                          $.fn.colorbox.close();

                          calEvent.title = result.title;
                          fullcalendar.fullCalendar('updateEvent',calEvent);
                          
                          if ($('#fullcalendar-status').text() === '') {
                            $('#fullcalendar-status').html(result.msg).slideDown();
                          } else {
                            $('#fullcalendar-status').html(result.msg).effect('highlight', {}, 5000);
                          }
                        }
                        return false;          
                      }
                    );
                    return false;
                  });
                }
              });
            //);
            } else {
              window.open(editEventUrl);
            }
          } else {
            window.open(editEventUrl);
          }
        }
        else if(action == 'delete') {
          if(!calEvent.deleteable) {
            return;
          }
          var deleteEventUrl = Drupal.settings.basePath + 'node/' + calEvent.nid + '/delete';          
          $.post(
            Drupal.settings.basePath + "fullcalendar/ajax/delete/" + calEvent.nid, 
            { 
              nid: calEvent.nid,
            },
            function(response) {
              var result = Drupal.parseJson(response);
              if(!result.status) {
                alert(Drupal.t('Error while deleting the event.'));  
              }
              else {
                fullcalendar.fullCalendar('removeEvents',function(event) {
                   return event.nid == calEvent.nid;
                });
                if ($('#fullcalendar-status').text() === '') {
                  $('#fullcalendar-status').html(result.msg).slideDown();
                } else {
                  $('#fullcalendar-status').html(result.msg).effect('highlight', {}, 5000);
                }
              }
            }
          );            
        }
        return false;
      }      
    );
    element.find('.fc-event-action').disableContextMenuItems(editMenu+', '+deleteMenu);
    /**/    
    }   
  /* End right click menu to edit and delete */
  });

  var fullcalendarUpdate = function(response) {
    var result = Drupal.parseJson(response);
    if ($('#fullcalendar-status').text() === '') {
      $('#fullcalendar-status').html(result.msg).slideDown();
    } else {
      $('#fullcalendar-status').html(result.msg).effect('highlight', {}, 5000);
    }
    return false;
  };

  $('.fullcalendar-status-close').live('click', function() {
    $('#fullcalendar-status').slideUp();
    return false;
  });

  //trigger a window resize so that calendar will redraw itself as it loads funny in some browsers occasionally
  $(window).resize();
};

})(jQuery);